/**
 * Leads.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var request = require('request');
module.exports = {

    tableName: 'leads',

    attributes: {

        projectname: {
            type: 'string',
            required: true
        },
        ownername: {
            type: 'string',
            required: true
        },
        projecttype: {
            type: 'string'
        },
        expectedtonnage: {
            type: 'string',
            defaultsTo: 0
        },
        pincode: {
            type: 'integer',
            defaultsTo: 0
        },
        state: {
            type: 'string',
            required: true
        },
        city: {
            type: 'string',
            required: true
        },
        // keycontacts: [{
        //     name: {
        //         type: 'string'
        //     },
        //     role: {
        //         type: 'string'
        //     },
        //     contact: {
        //         type: 'string'
        //     }
        // }],
        keycontacts: {
            type: 'array'
        },
        influencers: [{
            iid: {
                type: 'string'
            },
            weight: {
                type: 'integer',
                defaultsTo: 0
            }
        }],
        createdby: {
            name: {
                type: 'string'
            },
            contact: {
                type: 'string'
            },
            role: {
                type: 'string'
            }
        },
        createdate: {
            type: 'date'
        },
        inputmedium: {
            type: 'string'
        },
        assignedto: {
            assignid: {
                type: 'integer'
            },
            name: {
                type: 'string'
            }
        },
        status: {
            type: 'string',
            required: true
        },
        estimation: [{
            sku: {
                type: 'string'
            },
            quantity: {
                type: 'integer'
            },
            unit: {
                type: 'string'
            },
            dealer: {
                id: {
                    type: 'integer'
                },
                name: {
                    type: 'string'
                }
            }
        }],
        visit: [{
            purpose: {
                type: 'string'
            },
            visitdate: {
                type: 'date'
            },
            time: {
                type: 'date'
            },
            mode: {
                type: 'string'
            },
            contacts: [{
                name: {
                    type: 'string'
                },
                role: {
                    type: 'string'
                },
                contact: {
                    type: 'string'
                }
            }],
            team: [{
                name: {
                    type: 'string'
                },
                role: {
                    type: 'string'
                },
                contact: {
                    type: 'string'
                }
            }],
            info: {
                type: 'string'
            }
        }],
        history: [{
            modifiedby: {
                type: 'string'
            },
            modifystatus: {
                type: 'string'
            },
            modifieddate: {
                type: 'date'
            }
        }]
    },


    //   # Method: addLead
    // # Description: to add a Lead

    addLead: function(reqBody, programInfo, token, userLogObject, next) {

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        //Variable defined to create dataLogObject
        var dataLogObject = {
            "userId": userLogObject.userId,
            "requestApi": userLogObject.requestApi,
            "eventType": userLogObject.eventType,
            "collection": "Add Secondary Sale",
        }

        var recordInfo = reqBody;
        var leadId = "LI" + Date.now();
        recordInfo.leadId = leadId;

        console.log("recordInfo", recordInfo);
        // record to be inserted
        // var programId = reqBody.programId; // program id
        // var clientId = reqBody.clientId;

        // creating the record
        Leads.create(recordInfo).exec(function(err, record) {
            console.log("record", record, "error", err);
            if (err) { // handling for error
                responseObj.message = err;
                sails.log.error("Leads-Model>addLead>Leads.create", "Input:" + recordInfo, " error:" + err);
                // callback("error in function 2", "function 2 failed");
            } else if (!record) { // handling in case undefined/no object was returned
                responseObj.message = "undefined object was returned";
                sails.log.error("Leads-Model>addLead>Leads.create", "Input:" + recordInfo, " result:" + record);
                // callback("error in function 2", "function 2 failed");
            } else { // record created successfully  
                responseObj.statusCode = 0;
                responseObj.message = "Leads record successfully created";
                responseObj.result = record;
                dataLogObject.newValue = record;
                Leads.getAllParents(record, userLogObject, token);
                // LogService.addDataLog(dataLogObject, token);
                sails.log.info("Leads-Model>addLead>Leads.create", responseObj.message);
            }
            next(responseObj);
        }); // end of SecondarySales.create
    },

    // To get parent users
    getAllParents: function(reqBody, userLogObject, token, next) {
        console.log("111", reqBody);
        var userId = reqBody.providedby;
        var userType = reqBody.providedbyRole;
        var clientId = reqBody.clientId;
        var programId = reqBody.programId;
        var LeadId = reqBody.LeadId;

        var responseObj = {};
        var parentUsers = [];

        /********************************   REQUEST OBJECT FOR EXTERNAL API CALLS   ******************************/
        //Request api url and request object for Client User
        var apiUrl1 = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().getAllClientUsers.port +
            ServConfigService.getApplicationAPIs().getAllClientUsers.url;

        var requestObject1 = {
            clientId: clientId,
            serviceType: "client",
            frontendUserInfo: userLogObject
        }

        var requestOptions1 = {
            url: apiUrl1,
            method: ServConfigService.getApplicationAPIs().getAllClientUsers.method,
            headers: {
                'authorization': 'Bearer ' +
                    "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4NDA1NjQ0OTk3NCIsImlhdCI6MTQ4NDMwNTQyOH0.oVHd01FMVlR56c-4Maiw5TTYNO8yZNND1Iyw2w3hm0DqoAyGNtMXanGuTy_yPZzmVRJUWQgIbYpNVfO_cYOpdw"
            },
            json: requestObject1
        };

        //Request api url and request object for BE User
        var apiURL2 = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().getAllProgramUsers.port +
            ServConfigService.getApplicationAPIs().getAllProgramUsers.url;

        var requestObject2 = {
            clientId: clientId,
            programId: programId,
            serviceType: "programSetup",
            frontendUserInfo: userLogObject
        }

        var requestOptions2 = {
            url: apiURL2,
            method: ServConfigService.getApplicationAPIs().getAllProgramUsers.method,
            headers: {
                'authorization': 'Bearer ' +
                    "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4NDA1NjQ0OTk3NCIsImlhdCI6MTQ4NDMwNTQyOH0.oVHd01FMVlR56c-4Maiw5TTYNO8yZNND1Iyw2w3hm0DqoAyGNtMXanGuTy_yPZzmVRJUWQgIbYpNVfO_cYOpdw"
            },
            json: requestObject2
        };
        /****************************************************************************************************** */

        var requestOptions;
        var tempObj = {};


        var getParentUser = function(singleObject) {
            console.log("222", singleObject);
            if (singleObject.providedbyRole === "ClientUser") {
                requestOptions = requestOptions1;
                requestObject1.clientUserId = singleObject.providedby;
            } else if (singleObject.providedbyRole === "ChannelPartner") {
                requestOptions = requestOptions2;
                requestObject2.programUserId = singleObject.providedby;
            }
            console.log("333", requestOptions);
            request(requestOptions, function(error, response, body) {
                console.log("444", body.result, "555", error);
                if (error || body === undefined) { // error occured
                    responseObj.message = error;
                    callback(error);
                } else {
                    if (body.statusCode === 0) { // program found succesfully
                        responseObj.message = "Parent found";
                        if (singleObject.providedbyRole === 'ClientUser') {
                            if (body.result.parentUserId === 'None' || body.result.parentUserId === undefined) {
                                responseObj.result = parentUsers;
                                console.log("666", parentUsers);
                                updateLead();
                            } else {
                                parentUsers.push(body.result.parentUserId);
                                tempObj.providedby = body.result.parentUserId;
                                tempObj.providedbyRole = "ClientUser";
                                console.log("777", parentUsers);
                                getParentUser(tempObj);
                            }
                        } else {
                            // if user is mapped to parent; perform recursion
                            if (body.result[0]) {
                                if (body.result[0].parentUsersInfo) {
                                    parentUsers.push(body.result[0].parentUsersInfo[0].programUserId);
                                    tempObj.providedby = body.result[0].parentUsersInfo[0].programUserId;
                                    tempObj.providedbyRole = body.result[0].parentUsersInfo[0].userType;
                                    console.log("888", parentUsers);
                                    getParentUser(tempObj);
                                } else {
                                    updateLead();
                                }
                            } else {
                                updateLead();
                            }
                        }
                    } else if (body.statusCode === 2) {
                        responseObj.statusCode = 2;

                        //SHOULD ADD DEBIT PARTY AS PARENT TO CREDIT PARTY
                        updateLead();
                    } else {
                        responseObj = body;
                        updateLead();
                    }

                }
            });
        }

        var updateLead = function() {
            console.log("parentUsers", parentUsers);
            Leads.update({ LeadId: LeadId }, { parentUsers: parentUsers }).exec(function(err, records) {
                if (err) {
                    responseObj.message = err;
                    sails.log.error("Leads-Model>updateLeads>Leads.update ", " error:" + err);
                    callback("error in function 3", "function 3 failure");
                } else if (!records) { // handling in case undefined/no object was returned
                    responseObj.message = "undefined object was returned";
                    sails.log.error("Leads-Model>updateLeads>Leads.update ", " result:" + records);
                    callback("error in function 3", "function 3 failure");
                } else if (records.length === 0) { // handling in case records was not updated
                    responseObj.message = "Record not found!!";
                    sails.log.error("Leads-Model>updateLeads>Leads.update ", " result:" + records);
                    //console.log("error in function 3", "function 3 failure");
                } else { // record successfully updated
                    responseObj.statusCode = 0;
                    responseObj.result = records[0];
                    console.log("UPDATED UPDATED UPDATED");
                    sails.log.info("Leads-Model>updateLeads>Leads.update : Leads record updated successfully!!");
                }
            });
        }

        getParentUser(reqBody);
    },


    /*
     **  methodName: getLeads
     **  Description: to get Leads.



     */
    getLeads: function(requ, token, next) {
        console.log("request", requ);
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        } /*variable defined to send response*/

        var skip = requ.skip;
        var limit = requ.limit;
        var sort = requ.sort;
        // var countQueryString;

        // if (requ.providedby !== undefined) { //filter using providedby
        //     countQueryString.providedby = requ.providedby;
        // }

        // if (requ.leadId !== undefined) { //filter using leadId
        //     countQueryString.leadId = requ.leadId;
        // }

        var queryString = [{
            $match: {
                "$or": [{ "providedby": requ.providedby },
                    { "parentUsers": { "$in": [requ.providedby] } }

                ],
                "programId": requ.programId
            },
        }];
        var countQueryString = {
            "programId": requ.programId,
            $or: [{ "providedby": requ.providedby },
                { "parentUsers": { "$in": [requ.providedby] } }
            ]
        };
        // if (requ.providedby !== undefined) { //filter using providedby
        //          countQueryString.providedby = requ.providedby;
        //  }

        queryString.push({
            $group: {
                "_id": null,
                data: {
                    $push: "$$ROOT"
                }
            }
        }, { $unwind: "$data" })

        queryString.push({
            $lookup: {
                "from": "ProgramUsers",
                "localField": "data.providedby",
                "foreignField": "programUserId",
                "as": "programUserInfo"
            }
        })
        queryString.push({
            "$project": {
                "userName": "$programUserInfo.userDetails.userName",
                "data": 1
            }
        })
        if (requ.leadId) {
            queryString.push({
                $match: {
                    "leadId": requ.leadId
                },
            });
            countQueryString = {
                "leadId": requ.leadId
            }
        }

        if (requ.status) {
            queryString.push({
                $match: {
                    "status": requ.status
                },
            });
            countQueryString = {
                "status": requ.status
            }
        }
        if (sort !== undefined) {
            queryString.push({ $sort: sort });

        }
        if (skip !== undefined) {
            queryString.push({ $skip: skip });

        }
        if (limit !== undefined) {
            queryString.push({ $limit: limit });
        }

        Leads.native(function(err, collection) {
            if (err) {
                responseObj.message = err;
                next(responseObj);
            } else {
                collection.aggregate(queryString, function(err, records) {
                    console.log("records,err", records, err);
                    if (err) {
                        responseObj.message = err;
                        next(responseObj);
                    } else if (!records) {
                        responseObj.message = "undefined object was returned";
                        next(responseObj);
                    } else if (records.length > 0) {
                        responseObj.statusCode = 0;
                        responseObj.message = "Leads fetched successfully";
                        responseObj.result = records;
                        responseObj.queryString = queryString;

                        CommonOperations.findCount("Leads", countQueryString, token, function(response) {
                            console.log("Response******************************8", response);
                            if (response.statusCode === 0) {
                                responseObj.statusCode = 0;
                                // responseObj.message = "Count Fetched"
                                // responseObj.result = responseObj.count;
                                responseObj.countQueryString = countQueryString;
                                console.log("QueryStraing", responseObj.countQueryString);
                                responseObj.count = response.result; // adding count to responseobj
                            } else {
                                responseObj.message = "No Records Found";
                            }
                            next(responseObj);
                        }); // end of count code
                        // next(responseObj);
                    } else {
                        responseObj.statusCode = 2;
                        responseObj.message = "Records not found";
                        next(responseObj);
                    }
                })
            }
        })
    },


    /*
           # Method: updateSecondarySale
           # Description: to update a secondary sales
       */

    updateLead: function(reqBody, token, userLogObject, next) {

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        var programId = reqBody.programId;
        var clientId = reqBody.clientId;
        var programUserId = reqBody.providedby;
        var updateInfo = reqBody.updateInfo;
        var leadId = reqBody.leadId;
        // var dataLogObject = {
        //     "userId": userLogObject.userId,
        //     "requestApi": userLogObject.requestApi,
        //     "eventType": userLogObject.eventType,
        //     "collection": "NonTransactionalBoosters",
        // }

        // find criteria for update operation
        var findCriteria = {
            leadId: leadId
        };


        // updating the record
        console.log("updateInfo", updateInfo);
        Leads.update(findCriteria, updateInfo).exec(function(err, records) {
            console.log("update", err, records);
            if (err) {
                responseObj.message = err;
                sails.log.error("SecondarySales-Model>updateSecondarySales>SecondarySales.update ", "Input:" + updateInfo, " error:" + err);
                // callback("error in function 3", "function 3 failure");
            } else if (!records) { // handling in case undefined/no object was returned
                responseObj.message = "undefined object was returned";
                sails.log.error("SecondarySales-Model>updateSecondarySales>SecondarySales.update ", "Input:" + updateInfo, " result:" + records);
                // callback("error in function 3", "function 3 failure");
            } else if (records.length === 0) { // handling in case records was not updated
                responseObj.message = "Record not found!!";
                sails.log.error("SecondarySales-Model>updateSecondarySales>SecondarySales.update ", "Input:" + updateInfo, " result:" + records);
                // callback("error in function 3", "function 3 failure");
            } else { // record successfully updated
                responseObj.message = "Record updated!";
                responseObj.result = records;
                responseObj.statusCode = 0;

                console.log("Records", records);
                var length = responseObj.result.length;

                for (var a = 0; a < length; a++) {



                    var obj = {
                        "message": "",
                        "programId": responseObj.result[a].programId,
                        "clientId": responseObj.result[a].clientId,
                        "senderName": responseObj.result[a].senderName,
                        "senderRoleName": responseObj.result[a].senderRoleName,
                        "senderId": responseObj.result[a].senderId,
                        "receiverId": responseObj.result[a].providedby,
                        "type": "Lead",
                        "serviceType": "programOperations",
                        "frontendUserInfo": ""


                    }
                    if (responseObj.result[a].qualified === true) {
                        obj.message = " The status of your lead with lead Id " + responseObj.result[a].leadId + " has been changed to approved by " + responseObj.result[a].senderRoleName + " " + responseObj.result[a].senderName;
                    } else if (responseObj.result[a].rejected === true) {
                        obj.message = " The status of your lead with lead Id " + responseObj.result[a].leadId + " has been changed to rejected by " + responseObj.result[a].senderRoleName + " " + responseObj.result[a].senderName;
                    } else if (responseObj.result[a].pending === true) {
                        obj.message = " The status of your lead with lead Id " + responseObj.result[a].leadId + " has been changed to pending by " + responseObj.result[a].senderRoleName + " " + responseObj.result[a].senderName;
                    }

                    console.log("User Notification Object", obj);
                    console.log("Token", token);


                    Leads.addUserNotifications(obj, token, userLogObject, function(response) {

                        if (response.statusCode === 0) {
                            responseObj.message = "Lead user notification Added";
                            responseObj.result = records;
                            next(responseObj);


                        } else {
                            console.log("FAILURE");
                            responseObj.message = "Failed to add user notification of lead";
                            next(responseObj);
                        }


                    });
                }
            }
            //next(responseObj);
        });
    },

    /*
    	# Method: get Hall of Fame for Engineers
    	# Description: to fetch Hall of Fame for Engineers based on number of leads uploaded.
    	*/
    getHallOfFame: function(reqBody, token, next) {
        console.log("request", reqBody);
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        } /*variable defined to send response*/
        var startDate = new Date();
        startDate.setMonth(startDate.getMonth() - 1, 1);
        startDate.setDate(startDate.getDate() - (startDate.getDate() - 1))
        var endDate = new Date();
        endDate.setDate(endDate.getDate() - (endDate.getDate() - 1))
        var queryString = [
            // Stage 1
            {
                $match: {
                    qualified: true,
                    createdAt: {
                        $gte: startDate,
                        $lte: endDate
                    }
                },
            },
            // Stage 2
            {
                $project: {
                    "projectname": 1,
                    "ownername": 1,
                    "projecttype": 1,
                    "expectedvalue": 1,
                    "address": 1,
                    "city": 1,
                    "state": 1,
                    "pincode": 1,
                    "keyvontacts": 1,
                    "providedby": 1,
                    "providedbyRole": 1,
                    "providedbyName": 1,
                    "createdate": 1,
                    "qualified": 1,
                    "status": 1,
                    "programId": 1,
                    "clientId": 1,
                    "leadId": 1,
                    "createdAt": 1,
                    "updatedAt": 1,
                    "parentUsers": 1
                }
            },

            // Stage 3
            {
                $group: {
                    "_id": { "programUserId": "$providedby" },
                    "data": {
                        $push: "$$ROOT"
                    }
                }
            },

            // Stage 4
            {
                $project: {
                    "_id": 1,
                    "data": 1,
                    "numberOfLeads": { "$size": "$data" }
                }
            },

            // Stage 6
            {
                $lookup: {
                    "from": "ProgramUsers",
                    "localField": "_id.programUserId",
                    "foreignField": "programUserId",
                    "as": "programUserInfo"


                }
            },

            // Stage 7
            {
                $unwind: "$programUserInfo"
            },

            // Stage 8
            {
                $match: {
                    "programUserInfo.programId": "PR1503824208902"
                }
            },

            // Stage 9
            {
                $project: {
                    "_id": 1,
                    // "data": 1,
                    "numberOfLeads": 1,
                    //"userName": "$programUserInfo.userDetails.userName"
                    "programUserInfo": 1
                }
            },

            // Stage 10
            {
                $group: {
                    "_id": { "numberOfLeads": "$numberOfLeads" },
                    "programUserInfo": {
                        $push: "$programUserInfo"
                    }
                }
            },

            // Stage 11
            {
                $sort: {
                    "_id.numberOfLeads": -1
                }
            },

            // Stage 12
            {
                $limit: 5
            },

        ]




        Leads.native(function(err, collection) {
            if (err) {
                responseObj.message = err;
                next(responseObj);
            } else {
                collection.aggregate(queryString, function(err, records) {
                    console.log("records,err", records, err);
                    responseObj.queryString = queryString;
                    if (err) {
                        responseObj.message = err;
                        next(responseObj);
                    } else if (!records) {
                        responseObj.message = "undefined object was returned";
                        next(responseObj);
                    } else if (records.length > 0) {
                        responseObj.statusCode = 0;
                        responseObj.message = "Hall of Fame fetched successfully";
                        responseObj.result = records;
                        next(responseObj);
                    } else {
                        responseObj.statusCode = 2;
                        responseObj.message = "Records not found";
                        next(responseObj);
                    }
                })
            }
        })
    },




    /*
             # Method: countLeads
             # Description: to get count of leads
    */

    countLeads: function(reqBody, token, next) {

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        var programId = reqBody.programId;
        var clientId = reqBody.clientId;
        var providedby = reqBody.providedby;
        var startDate = reqBody.startDate
        var endDate = reqBody.endDate;
        var countQueryString = { qualified: reqBody.qualified };


        if (reqBody.startDate) {
            var startDate = new Date(startDate).toISOString();
            //startDate.setMonth(startDate.getMonth() - 1);
            //startDate.setDate(startDate.getDate() - (startDate.getDate() - 1))
        }
        if (reqBody.endDate) {
            var endDate = new Date(endDate).toISOString();
            //endDate.setDate(endDate.getDate() - (endDate.getDate() - 1))
        }

        if (reqBody.providedby !== undefined) { //filter using providedby
            countQueryString.providedby = reqBody.providedby;
        }

        if (reqBody.startDate !== undefined) { //filter using startDate

            countQueryString.createdAt = { $gte: startDate }

        }

        if (reqBody.endDate !== undefined) { //filter using endDate
            countQueryString.createdAt = { $lte: endDate }

        }

        if (reqBody.endDate !== undefined && reqBody.startDate !== undefined) {
            countQueryString.createdAt = { $gte: startDate, $lte: endDate }
        }

        console.log("startDate", startDate);
        console.log("endDate", endDate);
        console.log("ProgramUserId", providedby);
        // find criteria for count operation
        // var findCriteria = {
        //     createdAt: {
        //         $gte: startDate,
        //         $lte: endDate
        //     },
        //     providedby: providedby,
        //     // clientId: clientId,
        //     // programId: programId
        // };

        // console.log("findCriteria", findCriteria);


        CommonOperations.findCount("Leads", countQueryString, token, function(response) {
            // console.log("Response", );
            if (response.statusCode === 0) {
                responseObj.statusCode = 0;
                responseObj.message = "Count Fetched"
                responseObj.result = responseObj.count;
                responseObj.countQueryString = countQueryString;
                responseObj.count = response.result; // adding count to responseobj
            } else {
                responseObj.message = "No Records Found";
            }
            next(responseObj);
        }); // end of count code

        // Leads.count(findCriteria).exec(function(err, records) {
        //     console.log("count", err, records);
        //     if (err) {
        //         responseObj.message = err;
        //         //sails.log.error("SecondarySales-Model>updateSecondarySales>SecondarySales.update ", "Input:" + updateInfo, " error:" + err);
        //         // callback("error in function 3", "function 3 failure");
        //     } else if (!records) { // handling in case undefined/no object was returned
        //         responseObj.message = "undefined object was returned";
        //         //sails.log.error("SecondarySales-Model>updateSecondarySales>SecondarySales.update ", "Input:" + updateInfo, " result:" + records);
        //         // callback("error in function 3", "function 3 failure");
        //     } else if (records.length === 0) { // handling in case records was not updated
        //         responseObj.message = "Record not found!!";
        //         //sails.log.error("SecondarySales-Model>updateSecondarySales>SecondarySales.update ", "Input:" + updateInfo, " result:" + records);
        //         // callback("error in function 3", "function 3 failure");
        //     } else { // record successfully updated
        //         responseObj.message = "Record Found!";
        //         responseObj.result = records;
        //         responseObj.statusCode = 0;

        //     }
        //     next(responseObj);
        // });
    },




    //To Call addUserNotifications
    addUserNotifications: function(reqBody, token, userLogObject, next) {
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };


        var apiURL =
            ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().addUserNotifications.port +
            ServConfigService.getApplicationAPIs().addUserNotifications.url;

        //request options to add User Notifications
        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().addUserNotifications.method,
            headers: {
                authorization: "Bearer " + token
            },
            json: reqBody
        };
        console.log("requestOptions", requestOptions);
        request(requestOptions, function(error, response, body) {
            console.log("body ..............................", body);
            if (error || body === undefined) {
                responseObj.statusCode = 2;
                responseObj.message = "Error occurred while validating addUserNotifications!!";
            } else {
                if (body.statusCode === 0) {
                    responseObj.statusCode = 0;
                    responseObj.message = "User Notification Added successfully!!";
                    responseObj.result = body.result;
                } else if (body.statusCode === 2) {
                    responseObj.statusCode = 2;
                    responseObj.message = "Failed to add user notifications";
                } else {
                    responseObj = body;
                }
            }
            next(responseObj);
        });
    }



}














//     // getLeads: function(next) {
//     //     Leads.find()
//     //         .exec(function(err, res) {
//     //             if (err) next(err);
//     //             next(res);
//     //         });
//     // },

//     getLeadsSummary: function(next) {
//         Leads.find({ select: ['projectname', 'ownername', 'keycontacts', 'status'] })
//             .exec(function(err, res) {
//                 if (err) next(err);
//                 next(res);
//             });
//     },

//     getLead: function(index, next) {
//         Leads.findOne({ id: index })
//             .exec(function(err, res) {
//                 if (err) next(err);
//                 next(res);
//             });
//     },

//     // addLead: function(data, next) {
//     //     /*var fd = {"projectname":"Lead5","ownername":"asdf","projecttype":"asdf","expectedvalue":889,"city":"sadf","district":"sadfsad","state":"sadfasdf","pincode":987878,"keycontacts":[{"name":"asdf","role":"asdf","contact":"asdf"}],"inputmedium":"email","status":"new"};
//     // Leads.create(fd)
//     // 	.exec(function(err, res) {
//     // 		if(err) next(err);
//     // 		next(res);
//     // 	});*/
//     //     Leads.native(function(err, collection) {
//     //         collection.insert(data, function(err, result) {
//     //             if (err) next(err);
//     //             sails.log.info(result);
//     //             next(result);
//     //         });
//     //     });
//     // },

//     updateLead: function(index, data, next) {
//         var ObjectId = require('mongodb').ObjectID;
//         var query = { _id: new ObjectId(index) };
//         /*Leads.update(query, data)
//         	.exec(function(err, res) {
//         		if(err) next(err);
//         		next(res);
//         	});*/

//         Leads.native(function(err, collection) {
//             collection.update(query, { "$set": data }, { upsert: false, multi: true },
//                 function(err, result) {
//                     if (err) next(err);
//                     next(result);
//                 });
//         });
//         /*Leads.native(function(err,collection) {
//           collection.update(query,data, function(err, result) {
//             if(err) next(err);
//             sails.log.info(result);
//             next(result);
//           });
//         }); */
//     },

//     visitReport: function(next) {
//         Leads.native(function(err, collection) {
//             collection.aggregate([{
//                         $project: {
//                             'projectname': 1,
//                             'createdate': 1,
//                             'assignedto': 1,
//                             'status': 1,
//                             'ownername': 1,
//                             'city': 1,
//                             'state': 1,
//                             'pincode': 1,
//                             'visits': 1
//                         }
//                     },
//                     { $unwind: '$visits' }
//                 ],
//                 function(err, result) {
//                     if (err) next(err);
//                     next(result);
//                 });
//         });
//     },

//     influencerReport: function(next) {
//         Leads.native(function(err, collection) {
//             collection.aggregate([
//                     /*{$project: {'projectname':1,
//                                 'createdate':1,
//                                 'status':1,
//                                 'influencers':1}},*/
//                     { $unwind: '$influencers' },
//                     {
//                         $project: {
//                             'projectname': 1,
//                             'createdate': 1,
//                             'qualified': 1,
//                             'status': 1,
//                             'influencers.iid': 1,
//                             'influencers.weight': 1
//                         }
//                     },
//                     {
//                         $group: {
//                             _id: { influencer: "$influencers.iid" },
//                             lead: {
//                                 $push: {
//                                     projectname: "$projectname",
//                                     createdate: "$createdate",
//                                     qualified: "$qualified",
//                                     status: "$status",
//                                     weight: "$influencers.weight"
//                                 }
//                             }
//                         }
//                     }
//                     //{$project: {'projectname':1,'createdate':1,'status':1,'influencers.iid':1,'influencers.weight':1}}
//                     //{ $group : {_id: {influencer:"$influencers.iid"}, weights:{$sum:{weight:"$influencers.weight"}}, total : { $sum : 1 } }}
//                     //{ $group : {_id: {influencer:"$influencers.iid",status:"$status", weight:"$influencers.weight"}, total : { $sum : 1 } }},
//                     //{ $group : {_id: {influencer:"$influencers.iid",status:"$status"},total : { $sum : 1 } }},
//                     //{ $group: { _id: "$_id.influencer", statuses: { $push: { status: "$_id.status", total:"$total"}}, weights:{$push: {weight:"$_id.weight"}}}}
//                     //{ $group: { _id: "$_id.influencer", weights: { $push: { weight: "$_id.weight", total:"$total"}}}}
//                     //{ $group : {_id: {influencers:"$influencers.iid", weight:"$influencers.weight"}, total : { $sum : 1 } }},
//                     //{ $group: { _id: "$_id.influencers", weights: { $push: { weight: "$_id.weight"}}}}
//                     //{ $group : {_id: {influencers:"$influencers.iid", status:"$status"}, total : { $sum : 1 } }},
//                     //{ $group: { _id: "$_id.influencers", statuses: { $push: { status: "$_id.status", total:"$total"}}}}
//                 ],
//                 function(err, result) {
//                     if (err) next(err);
//                     next(result);
//                 });
//         });
//     },

//     deleteLead: function(index, next) {
//         var ObjectId = require('mongodb').ObjectID;
//         var query = { _id: new ObjectId(index) };
//         /*Leads.remove(query)
//         	.exec(function(err, res) {
//         		if(err) next(err);
//         		next(res);
//         	});*/

//         Leads.native(function(err, collection) {
//             collection.remove(query, function(err, result) {
//                 if (err) next(err);
//                 next(result);
//             });
//         });
//         /*Leads.native(function(err,collection) {
//           collection.update(query,data, function(err, result) {
//             if(err) next(err);
//             sails.log.info(result);
//             next(result);
//           });
//         }); */
//     }
// };