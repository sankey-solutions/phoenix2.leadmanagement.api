/**
 * LogService
 * @description      :: Approval Service to send request for approval for data transaction
 * @created          :: Swati
 * @Created  Date    :: 09/03/2017
 */

"use strict";

var request = require('request');
var diff = require('deep-diff').diff;

module.exports = {

    /**
     *  Method    :: addRecordForApproval
     *  Description :: Function defined to call Approval Service to add requested data for approval in approval collection
     *
     */

    addRecordForApproval: function(requestObject, userLogObject, token, next) {

        var responseObj = {
                "statusCode": -1,
                "message": null,
                "result": null
            }
            ////console.log("requestObject",requestObject);
        var apiUrl; //Url defined  for doing service call
        var requestOptions; //requestOptions to be send for service call
        requestObject.frontendUserInfo = userLogObject;
        var changedInfo = [];
        ////console.log("requestObject",requestObject);
        if (requestObject.recordType === "Update") {

            switch (requestObject.model) {

                case "ProgramScheme":
                    //console.log("model is programscheme");
                    break;

                case "BoosterRules":
                    //console.log("model is boosterRule");
                    break;

                default:

                    if (requestObject.objectOldInfo[0].frontendUserInfo !== undefined) {
                        delete requestObject.objectOldInfo[0].frontendUserInfo
                    }

                    if (requestObject.model === "ProgramRoleConfiguration") {
                        var result = diff(requestObject.objectOldInfo[0], requestObject.objectNewInfo.progRoleInfo);
                    } else {
                        if (requestObject.objectNewInfo)
                            var result = diff(requestObject.objectOldInfo[0], requestObject.objectNewInfo);
                    }

                    //console.log("result", result);

                    for (var i = 0; i < result.length; i++) {
                        var tempObject = {};
                        if (result[i].kind === "E") {
                            tempObject.fieldName = result[i].path[0];
                            tempObject.oldValue = result[i].lhs;
                            tempObject.newValue = result[i].rhs;
                            changedInfo.push(tempObject);
                        }

                        if (result[i].kind === "N") {
                            //console.log("path", result[i].path[0]);
                            if (result[i].path[0] !== "frontendUserInfo" && result[i].path[0] !== "serviceType" && result[i].path[0] !== "isApproved") {
                                tempObject.fieldName = result[i].path[0];
                                tempObject.oldValue = "";
                                tempObject.newValue = result[i].rhs;
                                changedInfo.push(tempObject);
                            }
                        }
                    }
                    requestObject.changedInfo = changedInfo;
                    //console.log("changedInfo", changedInfo);
            } //end of switch case
        }

        apiUrl = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().addRecordForApproval.port +
            ServConfigService.getApplicationAPIs().addRecordForApproval.url

        requestOptions = {
            url: apiUrl,
            method: ServConfigService.getApplicationAPIs().addRecordForApproval.method,
            headers: {
                'authorization': 'Bearer ' + token
            },
            json: requestObject
        };
        ////console.log(requestOptions);
        request(requestOptions, function(error, response, body) {
            //console.log("body", body);
            if (error || body === undefined) {
                //console.log("Record fetched successfully");
                responseObj.message = "Error occurred in service call";
                next(responseObj);
            } else if (body.statusCode === 0) {
                responseObj.statusCode = 0;
                responseObj.message = "Record added successfully !!";
                next(responseObj);
            } else {
                if (requestObject.recordType === "ADD") {
                    var modelName = requestObject.model;
                    var fieldName = requestObject.fieldName;
                    var id = requestObject.recordId;
                    //responseObj = body;
                    CommonOperations.deleteRecord(modelName, fieldName, id, token, function(res) {
                        if (res.statusCode === 0) {
                            //console.log("Deleted record since record was not added in approval table");
                        } else {
                            responseObj = response;
                        }
                        next(responseObj);
                    })
                } else {
                    ////console.log("Failed to fetch record");
                    responseObj = body;
                    next(responseObj);
                }
            }
        })
    },

    /**
     *  Method    :: setApprovalAddInfo
     *  Description :: to set request object for approval in approval collection
     *
     */
    setApprovalAddInfo: function(userInfo, response, next) {

        var userObject = {};
        var result = response.result;
        var approvalRequestObject = {};

        userObject.userId = userInfo.userId;
        userObject.userName = userInfo.fullName;

        var requestApi = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().updateProgramRecords.port +
            ServConfigService.getApplicationAPIs().updateProgramRecords.url // Generating the Url to consume API

        //handling requestObject to call approval model
        approvalRequestObject.userId = userInfo.userId;
        approvalRequestObject.userInfo = userObject;
        approvalRequestObject.isApproved = false;
        // //console.log("client id",result.clientId);
        approvalRequestObject.clientId = result.clientId;
        // //console.log("client id",approvalRequestObject.clientId)

        approvalRequestObject.API = requestApi;
        approvalRequestObject.objectNewInfo = result;
        approvalRequestObject.recordType = 'Add';
        approvalRequestObject.serviceType = 'programSetup';
        approvalRequestObject.methodType = ServConfigService.getApplicationAPIs().updateProgramRecords.method
        next(approvalRequestObject);
    },

    /**
     *  Method    :: setApprovalUpdatelInfo
     *  Description :: to set request object for approval in approval collection
     *
     */
    setApprovalUpdatelInfo: function(userInfo, approvalRequestObject, requestObject, token, next) {

        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        }

        var userObject = {};

        var Model = approvalRequestObject.model;
        var id = approvalRequestObject.recordId;
        var fieldName = approvalRequestObject.fieldName;

        userObject.userId = userInfo.userId;
        userObject.userName = userInfo.fullName;

        //handling requestObject to call approval model
        approvalRequestObject.userId = userInfo.userId;
        approvalRequestObject.userInfo = userObject;
        approvalRequestObject.isApproved = false;
        approvalRequestObject.programId = requestObject.programId;
        approvalRequestObject.clientId = requestObject.clientId;
        approvalRequestObject.recordType = 'Update';
        approvalRequestObject.serviceType = "programSetup";

        CommonOperations.findRecord(Model, id, fieldName, token, requestObject.programId, function(response) {
            if (response.statusCode === 0) {
                //storing old object
                approvalRequestObject.objectOldInfo = response.result;
                responseObj.statusCode = 0;
                responseObj.message = "Object found successfully";
                responseObj.result = approvalRequestObject;
            } else {
                //send response object with  message as no data found
                responseObj.statusCode = 2;
                responseObj.message = "Object not found;"
            }
            next(responseObj);
        })
    }
};