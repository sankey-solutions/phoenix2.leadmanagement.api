/**
 * EncryptDecrypt.js
 *
 * @description 	 :: Encryption & Decryption Service
 * @created          :: Chetan
 * @Created  Date    :: 17/10/2016
 * @lastEdited       :: Chetan
 * @lastEdited Date  :: 17/10/2016
 *
 */
 
"use strict";

var crypto = require('crypto');

module.exports = {

	key: "someKey", 

	encrypt: function(plainTextData) {
	    var cipher = crypto.createCipher('aes256', EncryptDecryptService.key);  
	    var encrypted = cipher.update(plainTextData, 'utf-8', 'hex');
	  
	    encrypted += cipher.final('hex');	   

	    return encrypted;
	},

	decrypt: function(encryptedData) {
	    var decipher = crypto.createDecipher('aes256', EncryptDecryptService.key);
	   
	    var decrypted = decipher.update(encryptedData, 'hex', 'utf-8'); 		

	    decrypted += decipher.final('utf-8');	    

	    return decrypted;
	}
};